# -*- coding: utf-8 -*-

"""
Los views son los módulos que funcionan como un controlador

Teóricamente este debería de hacer una petición a Elastic y mostrarlo en el
navegador
"""

import random, math, re

from django.shortcuts import render
from django.views.defaults import page_not_found
from django.contrib.staticfiles.templatetags.staticfiles import static
from elasticsearch import Elasticsearch
from settings import *

# Crea una conexión a elastic search
elastic = Elasticsearch([{'host': INDEX_HOST, 'port': INDEX_PORT}])

# Obtiene los parámetros pasados por el método get
def getParameters(request):
	return {
		"query": request.GET["q"] if "q" in request.GET else "",
		"title": request.GET["title"] if "title" in request.GET else "",
		"tags": request.GET["tags"] if "tags" in request.GET else "",
		"expand": request.GET["expand"] if "expand" in request.GET else "false",
		"origin": request.GET["origin"] if "origin" in request.GET else "",
		"optionalOrigin": True if "optionalOrigin" in request.GET else False,
		"year": request.GET["year"] if "year" in request.GET else "",
		"optionalYear": True if "optionalYear" in request.GET else False,
		"status": request.GET["status"] if "status" in request.GET else "All",
		"category": request.GET["category"] if "category" in request.GET else "All",
		"aggregation": request.GET["aggregation"] if "aggregation" in request.GET else "",
		"page": int(request.GET["page"]) if "page" in request.GET else 1
	}

# Obtiene resultados de búsqueda
def getSearchBody(params):
	# Creamos el cuerpo de la petición
	body = {
		"query": {
			"bool": {
				"must": [],
				"should": []
			}
		}
	}

	# Comprobamos si hay query
	if params["query"] != "":
		body["query"]["bool"]["must"].append({
			"multi_match": {
				"query": params["query"],
				"type": "cross_fields",
				"fields": ["meta_og:title^5", "meme_tags.text^3", "meme_content"],
				"operator": "and"
			}
		})
		body["highlight"] = {
			"fields" : {
				"meta_og:title": {},
				"meme_tags.text": {},
				"meme_content": {}
			}
		}

	# Se comprueba el título
	if params["title"] != "":
		body["query"]["bool"]["must"].append({
			"match": {
				"meta_og:title": {
					"query": params["title"],
					"operator": "and"
				}
			}
		})
		if not "highlight" in body:
			body["highlight"] = {
				"fields": {
					"meta_og:title": {}
				}
			}

	# Se comprueban las tags
	if params["tags"] != "":
		tagsSplit = params["tags"].split(",")
		for tag in tagsSplit:
			body["query"]["bool"]["must"].append({
				"match": {
					"meme_tags.text": {
						"query": params["tags"].strip(),
						"operator": "and"
					}
				}
			})
			if not "highlight" in body:
				body["highlight"] = {
					"fields": {
						"meme_tags.text": {}
					}
				}
			elif not "meme_tags.text" in body["highlight"]["fields"]:
				body["highlight"]["fields"]["meme_tags.text"] = {}

	# Si no hay query ni restricciones de título o queries, ordenar alfabéticamente
	# (estarán pidiendo todos los que pasen unos determinados filtros)
	if params["query"] == "" and params["title"] == "" and params["tags"] == "":
		body["sort"] = []
		body["sort"].append("meta_og:title.keyword")

	# Se comprueba el origen
	if params["origin"] != "":
		originRequest = {
			"match": {
				"meme_origin": {"query": params["origin"]}
			}
		}
		if params["optionalOrigin"]:
			originRequest["match"]["meme_origin"]["boost"] = 3
			body["query"]["bool"]["should"].append(originRequest)
		else:
			body["query"]["bool"]["must"].append(originRequest)

	# Se comprueba el año
	if params["year"] != "":
		splitedYear = params["year"].split("-")
		yearRequest = None
		typeOfRequest = None
		if len(splitedYear) == 1:
			yearRequest = {
				"term": {
					"meme_year": {
						"value": params["year"].strip()
					}
				}
			}
			typeOfRequest = "term"
		# La búsqueda por rangos de año es inclusiva por los dos extremos
		else:
			yearRequest = {
				"range": {
					"meme_year": {
						"gte": splitedYear[0].strip(),
						"lte": splitedYear[1].strip()
					}
				}
			}
			typeOfRequest = "range"
		if params["optionalYear"]:
			yearRequest[typeOfRequest]["meme_year"]["boost"] = 3
			body["query"]["bool"]["should"].append(yearRequest)
		else:
			body["query"]["bool"]["must"].append(yearRequest)

	# Se comprueba el estado
	if params["status"] != "All":
		body["query"]["bool"]["must"].append({
			"term": {
				"meme_status": {
					"value": params["status"]
				}
			}
		})

	# Se comprueba la categoria
	if params["category"] != "All":
		body["query"]["bool"]["must"].append({
			"term": {
				"meme_category": {
					"value": params["category"]
				}
			}
		})

	# Comprobamos si alguna de las cláusulas está vacía
	existsMust = True if len(body["query"]["bool"]["must"]) > 0 else False
	existsShould = True if len(body["query"]["bool"]["should"]) > 0 else False

	# Si ambas están vacías, reemplazamos la query por un match_all
	if not existsMust and not existsShould:
		del body["query"]["bool"]
		body["query"] = {"match_all": {}}
	elif not existsMust:
		del body["query"]["bool"]["must"]
	elif not existsShould:
		del body["query"]["bool"]["should"]

	# Se obtiene la página
	if params["page"] > 1:
		body["from"] = (params["page"]-1)*10

	print(params["page"])

	return body

# Obtiene los resultados de las agregaciones
def getAggregationResults(body, params):
	aggregations = {}

	# Si hay agregaciones obtenemos esos valores
	if params["aggregation"] != "":
		body_copy = body.copy()

		pos = 0
		while ("bool" in body_copy["query"]) and (pos < len(body_copy["query"]["bool"]["must"])):
			query = body_copy["query"]["bool"]["must"][pos]
			if ("term" in query and "meme_status" in query["term"]) or ("term" in query and "meme_category" in query["term"]):
				del body_copy["query"]["bool"]["must"][pos]
			else:
				pos += 1

		body_copy["size"] = 0
		anotherAggregation = "category" if params["aggregation"] == "status" else "status"

		body_copy["aggs"] = {
			"group_by_" + params["aggregation"]: {
				"terms": {
					"field": "meme_" + params["aggregation"]
				},
				"aggs": {
					"group_by_" + anotherAggregation: {
						"terms": {
							"field": "meme_" + anotherAggregation
						}
					}
				}
			}
		}

		# Hacemos una búsqueda
		result = elastic.search(index=INDEX_NAME, body=body_copy)

		# Recorremos los resultados
		for bucket in result["aggregations"]["group_by_" + params["aggregation"]]["buckets"]:
			aggregations[bucket["key"].lower()] = bucket["doc_count"]

			# Si estamos en el marcado, obtenemos sus hijos
			if (bucket["key"] == params["status"]) or (bucket["key"] == params["category"]):
				for bucketChild in bucket["group_by_" + anotherAggregation]["buckets"]:
					aggregations[bucketChild["key"].lower()] = bucketChild["doc_count"]
	else:
		# Obtenemos las agregaciones de status
		body_status = body.copy()
		body_status["size"] = 0
		body_status["aggs"] = {
			"group_by_status": {
				"terms": {
					"field": "meme_status"
				}
			}
		}
		status_result = elastic.search(index=INDEX_NAME, body=body_status)

		for bucket in status_result["aggregations"]["group_by_status"]["buckets"]:
			aggregations[bucket["key"].lower()] = bucket["doc_count"]

		# Obtenemos las agregaciones de category
		body_category = body.copy()
		body_category["size"] = 0
		body_category["aggs"] = {
			"group_by_category": {
				"terms": {
					"field": "meme_category"
				}
			}
		}
		status_category = elastic.search(index=INDEX_NAME, body=body_category)

		for bucket in status_category["aggregations"]["group_by_category"]["buckets"]:
			aggregations[bucket["key"].lower()] = bucket["doc_count"]

	return aggregations

# Creamos la lista de resultados
def generateHitList(context, results):
	for result in results["hits"]["hits"]:
		hit = {
			"title": result["_source"]["meta_og:title"],
			"description": result["_source"]["meta_description"],
			"url": result["_source"]["url"],
			"image": result["_source"]["meta_og:image"],
			"category": "https://img.shields.io/badge/-" + result["_source"]["meme_category"] + "-{0}.svg",
			"status": "https://img.shields.io/badge/-" + result["_source"]["meme_status"] + "-{0}.svg",
			"origin": result["_source"]["meme_origin"],
			"tags": "",
			"year": result["_source"]["meme_year"]
		}

		if isinstance(result["_source"]["meme_tags"], list):
			for tag in result["_source"]["meme_tags"]:
				if hit["tags"] != "":
					hit["tags"] += ", "
				hit["tags"] += tag
		else:
			hit["tags"] = result["_source"]["meme_tags"]

		# TODO para el contenido mostrar directamente los fragmentos que devuelve el highlight
		if "highlight" in result:
			highlights = result["highlight"]
			# Highlightear título
			if "meta_og:title" in highlights:
				for highlight in highlights["meta_og:title"]:
					matches = re.findall("<em>(.+?)</em>", highlight)
					for i in range(len(matches)):
						hit["title"] = hit["title"].replace(matches[i], "<em>" + matches[i] + "</em>")
			# Highlightear tags
			# TODO no creo que sea especialmente eficiente
			if "meme_tags.text" in highlights:
				for highlight in highlights["meme_tags.text"]:
					matches = re.findall("<em>(.+?)</em>", highlight)
					for i in range(len(matches)):
						if isinstance(hit["tags"], list):
							for j in range(len(hit["tags"])):
								hit["tags"][j] = hit["tags"][j].replace(matches[i], "<em>" + matches[i] + "</em>")
						else:
							hit["tags"] = hit["tags"].replace(matches[i], "<em>" + matches[i] + "</em>")
			# Highlightear content
			if "meme_content" in highlights:
				hit["matchedcontent"] = []
				if isinstance(highlights["meme_content"], list):
					for match in highlights["meme_content"]:
						hit["matchedcontent"].append(match)
				else:
					hit["matchedcontent"].append(highlights["meme_content"])

		# Ponemos de color el badge de categoría
		if result["_source"]["meme_category"] == "Culture":
			hit["category"] = hit["category"].format("1aa2cb")
		elif result["_source"]["meme_category"] == "Subculture":
			hit["category"] = hit["category"].format("5d1cb1")
		elif result["_source"]["meme_category"] == "Event":
			hit["category"] = hit["category"].format("0b6a40")
		elif result["_source"]["meme_category"] == "Meme":
			hit["category"] = hit["category"].format("220a51")
		elif result["_source"]["meme_category"] == "Person":
			hit["category"] = hit["category"].format("d32f2e")
		elif result["_source"]["meme_category"] == "Site":
			hit["category"] = hit["category"].format("e09f02")

		# Ponemos el color en el badge de estado
		if result["_source"]["meme_status"] == "Confirmed":
			hit["status"] = hit["status"].format("00cc00")
		elif result["_source"]["meme_status"] == "Submission":
			hit["status"] = hit["status"].format("cc8800")
		elif result["_source"]["meme_status"] == "Deadpool":
			hit["status"] = hit["status"].format("cc0000")

		context["hits"].append(hit)

'''
--------------------------------------------------------------------------------
	Páginas principales
--------------------------------------------------------------------------------
'''

# Página principal
def home(request):
	'''
		Crea la página de inicio
	'''
	context = {}

	# Mensaje de nuevo
	if 'nue' not in request.COOKIES:
		context['nue'] = True

	response = render(request, "index.html", context)

	# Se crea la cookie si no estas
	if 'nue' not in request.COOKIES:
		response.set_cookie('nue', 'true')

	return response

# Página de búsqueda
def search(request):
	# Obtenemos los elementos de la petición get
	params = getParameters(request)

	# Obtenemos el body
	body = getSearchBody(params)

	print(body)

	# Resultados de la búsqueda
	results = elastic.search(index=INDEX_NAME, body=body)

	# Número de páginas totales
	pages = int(math.ceil(results["hits"]["total"] / 10.0))

	# Debugging
	print("results: " + str(results["hits"]["total"]) + "\tpages: " + str(pages))

	# Creamos el contexto
	context = {
		"query": params["query"],
		"expand": params["expand"],
		"title": params["title"],
		"tags": params["tags"],
		"origin": params["origin"],
		"optionalOrigin": params["optionalOrigin"],
		"year": params["year"],
		"optionalYear": params["optionalYear"],
		"status": params["status"],
		"category": params["category"],
		"aggregation": params["aggregation"],
		"aggregations": getAggregationResults(body, params),
		"hits": [],
		"pages": [],
		"page": params["page"],
		"totalPages": pages
	}

	# Comprobamos los resultados
	if results["hits"]["total"] == 0:
		return render(request, "search_404.html", context)
	else:
		# Obtenemos los resultados
		generateHitList(context, results)

		# Si se puede paginar, creamos la listas e páginas
		if results["hits"]["total"] > 10:
			min_pos = max(params["page"]-2, 2)
			max_pos = min(min_pos+5, pages)

			# Comprobamos si hay que poner puntos suspensivos
			if min_pos > 2:
				context["minPageSkipped"] = True
			if max_pos < pages-2:
				context["maxPageSkipped"] = True

			context["pages"].append(1)
			for i in range(min_pos, max_pos):
				context["pages"].append(i)
			context["pages"].append(pages)

		return render(request, "search.html", context)

# Página 404
def handler404(request):
	# Obtiene un número aleatorio
	n = random.randint(1, 4)
	# Obtiene una imagen aleatoria
	response = render(request, "404.html", {
		"image": static('/images/404_' +  str(n) + '.png')
	})
	response.status_code = 404
	return response
