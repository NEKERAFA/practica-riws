function onPressMore () {
  // Estado de expandir
  var expandStatus = document.getElementsByName('expand')[0]
  // Caja de búsqueda avanzada
  var divAdvance = document.getElementById('advance-search')
  // Botón de expandir
  var buttonMore = document.getElementById('more')

  // Miramos el estado y lo cambiamos
  if (expandStatus.value === 'true') {
    expandStatus.value = 'false'
    divAdvance.className = 'hidden'
    buttonMore.value = 'expand_more'
  } else {
    expandStatus.value = 'true'
    divAdvance.className = 'hidden open'
    buttonMore.value = 'expand_less'
  }
}

function onContentMatches(button) {
  var div = button.nextElementSibling

  console.log(div)

  if (button.dataset.open === "true") {
    button.innerHTML = "Content matches +"
    div.style.display = "none"
    button.dataset.open = "false"
  } else {
    button.innerHTML = "Content matches -"
    div.style.display = "block"
    button.dataset.open = "true"
  }
}

/* Función para comprobar que un parámetro no esté por defecto */
function checkParameter (param) {
  // Comprobación de select
  if ((param.tagName === 'SELECT') && (param.options[param.selectedIndex].value === 'All')) {
    return false
  }

  // Comprobación de checkbox
  if ((param.type === 'checkbox') && (!param.checked)) {
    return false
  }

  // Comprobación del parámetro hidden
  if ((param.type === 'hidden') && (param.value === 'false')) {
    return false
  }

  // Comprobación de cualquier input
  if (param.value === '') {
    return false
  }

  return true
}

// Función para crear el string que irá en la url del parámetro
function parameterUrl (param) {
  var paramStr = param.name + '='

  // Si es un select se añade el campo valor de la opción seleccionada a la ruta
  if (param.tagName === 'SELECT') {
    return paramStr + param.options[param.selectedIndex].value
  }

  // Si es un checkbox se mira el checked
  if (param.type === 'checkbox') {
    return paramStr + param.checked
  }

  // Si es un input normal se mira el value
  return paramStr + param.value
}

function buildUrl () {
  var params = document.getElementsByClassName('param')
  var url = '?'

  // Recorremos la lista de elementos para pasar por parámetro
  for (var i = 0; i < params.length; i++) {
    var param = params[i]

    // Comprobamos si los parámetros no están vacios no están vacios
    if (checkParameter(param)) {
      // Comprobamos si no es el primer parámetro para añadirle antes un &
      if (i > 0) {
        url += '&'
      }

      url += parameterUrl(param)
    }
  }

  return url !== '?' ? url : ''
}

function checkDate(date) {
  if (date == null) {
    date = document.getElementsByName("year")[0]
  }

  return /^(\s*\d+\s*(-\s*\d+\s*)?)?$/.test(date.value)
}

function printValidation(date) {
  console.log(checkDate(date))

  if (checkDate(date)) {
    date.classList.remove("error")
  } else {
    date.classList.add("error")
  }
}

function concatUrlParams(base, params) {
  result = ''

  if (base === '') {
    for (var i = 0; i < params.length; i++) {
      if (i === 0) {
        result += '?' + params[i]
      } else {
        result += '&' + params[i]
      }
    }
  } else {
    for (var i = 0; i < params.length; i++) {
      result += '&' + params[i]
    }
  }

  return result
}

// Se lanza al buscar
function onSearch () {
  if (checkDate()) {
    var url = buildUrl()
    var aggregation = document.getElementById("aggregation") ? document.getElementById("aggregation").value : ""
    var category = document.getElementById("category") ? document.getElementById("category").value : "All"
    var status = document.getElementById("status") ? document.getElementById("status").value : "All"

    if (aggregation !== "") {
      url += concatUrlParams(url, ["aggregation=" + aggregation])
    }

    if (category !== "All") {
      url += concatUrlParams(url, ["category=" + category])
    }

    if (status !== "All") {
      url += concatUrlParams(url, ["status=" + status])
    }

    location.assign('/memesearch/search' + url)
  } else {
    alert("The input year is incorrect")
  }
}

// Se lanza en la paginación
function onPage (page) {
  if (checkDate()) {
    var url = buildUrl()
    var aggregation = document.getElementById("aggregation") ? document.getElementById("aggregation").value : ""
    var category = document.getElementById("category") ? document.getElementById("category").value : "All"
    var status = document.getElementById("status") ? document.getElementById("status").value : "All"

    if (aggregation !== "") {
      url += concatUrlParams(url, ["aggregation=" + aggregation])
    }

    if (category !== "All") {
      url += concatUrlParams(url, ["category=" + category])
    }

    if (status !== "All") {
      url += concatUrlParams(url, ["status=" + status])
    }

    url += concatUrlParams(url, ["page=" + page])

    location.assign('/memesearch/search' + url)
  } else {
    alert("The input year is incorrect")
  }
}

// Se lanza en los filtros
function onFilter (key, value) {
  if (checkDate()) {
    var url = buildUrl()
    var aggregation = document.getElementById("aggregation").value
    var category = document.getElementById("category").value
    var status = document.getElementById("status").value

    // Si el elemento está marcado
    if ((key === "category" && category === value) || (key === "status" && status === value)) {
      // Si el elemento no es de la agregación primaria
      if (aggregation !== key) {
        // Establecemos la agregación
        if (aggregation === "") {
          url += concatUrlParams(url, ["aggregation=" + key])
        } else {
          url += concatUrlParams(url, ["aggregation=" + aggregation])
          // Establecemos el primer elemento
          if (key === "category") {
            url += concatUrlParams(url, ["status=" + status])
          } else {
            url += concatUrlParams(url, ["category=" + category])
          }
        }
      }
    } else {
      // Establecemos la agregación
      if (aggregation === "") {
        url += concatUrlParams(url, ["aggregation=" + key])
      } else {
        url += concatUrlParams(url, ["aggregation=" + aggregation])
        // Establecemos el primer elemento
        if (key === "category" && aggregation !== key) {
          url += concatUrlParams(url, ["status=" + status])
        } else if (key === "status" && aggregation !== key) {
          url += concatUrlParams(url, ["category=" + category])
        }
      }

      url += concatUrlParams(url, [key + "=" + value])
    }

    location.assign('/memesearch/search' + url)
  } else {
    alert("The input year is incorrect")
  }
}

// Limpia el contenido de un parámetro
function parameterClear (param) {
  if (param.tagName === 'SELECT') {
    param.selectedIndex = 0
  } else if (param.type === 'checkbox') {
    param.checked = false
  } else if (param.type !== 'hidden') {
    param.value = ""
  }
}

function deletThis() {
  var params = document.getElementsByClassName('param')

  // Recorremos la lista de elementos
  for (var i = 0; i < params.length; i++) {
    parameterClear(params[i])
  }
}

// Se consigue el input de búsqueda
var inputs = document.getElementsByTagName('INPUT')

// Se añade un listener a los inputs
for (var i = 0; i < inputs.length; i++) {
  if (inputs[i].type === "text" || inputs[i].type === "search" || inputs[i].type === "date") {
    inputs[i].addEventListener('keyup', function (event) {
      // Se cancela la acción por defecto
      event.preventDefault()
      // El número 13 es la tecla "Enter"
      if (event.keyCode === 13) {
        document.getElementById('submit').click()
      }
    })
  }
}
