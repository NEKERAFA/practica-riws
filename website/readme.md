# Instalación
* pip
	* pip install django
	* pip install elasticsearch
* anaconda
	* conda install -c anaconda django
	* conda install -c anaconda elasticsearch

# Ejecutar
	cd website
	python manage.py runserver
