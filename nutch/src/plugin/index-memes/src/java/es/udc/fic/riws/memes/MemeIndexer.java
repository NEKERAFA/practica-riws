package es.udc.fic.riws.memes;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;

import org.apache.avro.util.Utf8;
import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.indexer.IndexingException;
import org.apache.nutch.indexer.IndexingFilter;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.util.Bytes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemeIndexer implements IndexingFilter {

    // Logger
	private static final Logger LOG = LoggerFactory.getLogger(MemeIndexer.class.getName());

    private Configuration conf;

    // Campos a indexar
    private static Set<String> parseFieldnames = new HashSet<String>(Arrays.asList("meme_category", "meme_status", "meme_year", "meme_origin", "meme_tags", "meme_content"));

    public NutchDocument filter(NutchDocument doc, String url, WebPage page)
        throws IndexingException {

        // just in case
        if (doc == null)
            return doc;

        // Añadir los campos a indexar
        if (parseFieldnames != null) {
            for (String field : parseFieldnames) {
                LOG.info("Field: " + field);
                // Si no se usa un Utf8 como clave, funciona en el comando indexchecker pero no cuando se indexa de verdad con index -all
                ByteBuffer bvalues = page.getMetadata().get(new Utf8(field));
                if (bvalues != null) {
                    String value = Bytes.toString(bvalues.array());
                    LOG.info("Value: " + value);
                    // Sección para campos multivaluados
                    String[] values = value.split("\t");
                    for (String eachvalue : values) {
                        doc.add(field, eachvalue);
                    }
                    LOG.info("Final value size: " + doc.getFieldValues(field).size());
                } else {
                    LOG.info("Well shit");
                }
            }
        }

        return doc;
    }

    public void setConf(Configuration conf) {
        this.conf = conf;
    }

    public Configuration getConf() {
        return this.conf;
    }

    @Override
    public Collection<Field> getFields() {
        return null;
    }
}
