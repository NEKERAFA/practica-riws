package es.udc.fic.riws.memes;

import java.io.PrintWriter;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Map;
import java.util.HashSet;
import java.util.Iterator;
import org.apache.avro.util.Utf8;
import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.parse.*;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.storage.WebPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DocumentFragment;

/**
 * The Ultimate Meme Parser
 *
 * Eva Suárez García
 * Rafael Alcalde Azpiazu
 */

public class MemeParser implements ParseFilter {
	// Logger
	private static final Logger LOG = LoggerFactory.getLogger(MemeParser.class.getName());
	// Configuración
	private Configuration conf;
	// Prefijo que se le indica a los campos para que se indexen
	private static final String PARSE_MEMEDATA_PREFIX = "meme_";

	/**
	 * Lee la configuración y la guarda
	 */
	public void setConf(Configuration conf) {
		this.conf = conf;
	}

	/**
	 * Obtiene la configuración del plugin
	 */
	public Configuration getConf() {
		return this.conf;
	}

    @Override
    public Collection<Field> getFields() {
        return null;
    }

	/**
	 * Añade la información del meme como metadata
	 */
	private void addIndexedMemedata(Map<CharSequence, ByteBuffer> metadata,
			String memedata, String value) {
		LOG.info("Found " + PARSE_MEMEDATA_PREFIX + memedata + ": " + value);
		metadata.put(PARSE_MEMEDATA_PREFIX + memedata, ByteBuffer.wrap(value.getBytes()));
	}

	/**
	 * Función de parsing
	 */
	public Parse filter(String url, WebPage page, Parse parse,
	                    HTMLMetaTags metaTags, DocumentFragment doc) {

		try {
			// Obtenemos la codificación de UTF-8
			Charset utf8 = Charset.forName("UTF-8");

			// Obtenemos el contenido de la página descargada y convertimos de byte
			// buffer a byte array para convertirlo a string
			byte[] docBytes = page.getContent().array();
			String docString = new String(docBytes, utf8);

			// Parseamos el contenido con jsoup
			Document docHtml = Jsoup.parse(docString, page.getBaseUrl().toString());

			// Vamos bajando por la jerarquía para obtener el div central
			Element contentElement = docHtml.getElementById("entry_body");

            // ----- Obtenemos el aside con los datos del meme -----
			Element asideElement = contentElement.getElementsByTag("aside").get(0);

			// Obtenemos la categoría
			Element categoryElement = asideElement.getElementsByClass("entry-category-badge").get(0);
			addIndexedMemedata(page.getMetadata(), "category", categoryElement.ownText());

			// Recorremos los dl para obtener el resto de datos
			Elements siblingElements = categoryElement.siblingElements();
			Iterator<Element> siblingIter = siblingElements.iterator();
            int foundMemeData = 0; // Contador de datos encontrados, así podemos salir tan pronto encontremos los datos que buscamos

			while (siblingIter.hasNext() && foundMemeData < 3) {
				// Cogemos el título del dato
				Element titleElement = siblingIter.next();
				String title = titleElement.text();
				// Cogemos el valor del dato
				Element valueElement = siblingIter.next();
				String value = valueElement.text();

				if (title.contains("Status")) {
                    addIndexedMemedata(page.getMetadata(), "status", value);
                    foundMemeData++;
				} else if (title.contains("Year")) {
                    try {
                        Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        value = "-1";
                    }
					addIndexedMemedata(page.getMetadata(), "year", value);
                    foundMemeData++;
				} else if (title.contains("Origin")) {
					addIndexedMemedata(page.getMetadata(), "origin", value);
                    foundMemeData++;
				}
			}

            Element tagsElement = asideElement.getElementById("entry_tags").getElementsByTag("dd").get(0);
            String tags = "";
            for (Element tag : tagsElement.children()) {
                if (!tags.equals("")) {
                    tags += "\t";
                }
                tags += tag.ownText();
            }
            addIndexedMemedata(page.getMetadata(), "tags", tags);

            // ----- Obtenemos el texto principal del meme -----
            String memeText = "";
            Elements pElements = contentElement.getElementsByTag("p");
            for (Element pElement : pElements) {
                memeText += pElement.text();
            }
            addIndexedMemedata(page.getMetadata(), "content", memeText);

		} catch (Exception e) {
			LOG.error(e.getMessage());
		}

		return parse;
	}
}
